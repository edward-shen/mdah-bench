#!/usr/bin/env bash

envsubst <settings.sample.yaml >settings.yaml

./mangadex-home --quiet --quiet -Z disable-token-validation >run.log 2>&1 &

export CLIENT_PID
CLIENT_PID=$(pgrep -f "mangadex-home")

echo "Client started at pid $CLIENT_PID"
